require 'capybara'
require 'capybara/dsl'

Capybara.run_server = false
Capybara.current_driver = :selenium
Capybara.app_host = "https://www.google.com.tw"
Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end


module MyCapybara
  class Crawler
    include Capybara::DSL
    def query(params)
      visit("/")
      fill_in "q", with: params
      click_button "Google 搜尋"
      return all(:xpath, '//h3/a')
    end
  end
end

crawler = MyCapybara::Crawler.new
item = crawler.query('ruby')
item.each do |r|
  puts "#{r.text} => #{r[:href]}"
end
